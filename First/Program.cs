﻿using System;

namespace First
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Clear();
            while (true)
            { // бесконечный цикл;
                try
                {
                    Console.WriteLine("Введите элементы уравнения а*x^2 + b*x + c = 0");
                    Console.WriteLine("Введите а");
                    Double a = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите b");
                    Double b = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Введите c");
                    Double c = Convert.ToDouble(Console.ReadLine()); //ввод а, b, c;
                    Double D = Math.Pow(b, 2) - (4 * a * c); // дискриминант

                    if (D == 0 && a != 0) // d=0, a!=0 - один корень (два одинаковых);
                    {
                        Double x = (-b) / (2 * a);
                        Console.WriteLine("x = " + Math.Round(x, 3));
                        Console.WriteLine();

                    }
                    else if (D > 0 && a != 0) // d>0, a!=0 - два разных корня;
                    {
                        Double x1 = (Math.Sqrt(D) - b) / (2 * a);
                        Double x2 = (-Math.Sqrt(D) - b) / (2 * a);
                        Console.WriteLine("x1 = " + Math.Round(x1, 3) + "; x2 = " + Math.Round(x2, 3));
                        Console.WriteLine();

                    }

                    else if (D < 0 && a != 0) // // d<0, a=0 - два корня с комплексными числами;
                    {
                        Console.WriteLine("x1 = " + Math.Round((-b / (2 * a)), 3) + " + " + Math.Round(Math.Sqrt(Math.Abs(D)), 3) + "i" + "; x2 = " + Math.Round((-b / (2 * a)), 3) + " - " + Math.Round(Math.Sqrt(Math.Abs(D)), 3) + "i");
                        Console.WriteLine();
                    }
                    else if (a == 0) // а=0, линейное уравнение с одним корнем;
                    {
                        Console.WriteLine(" x = " + Math.Round((-c / b), 3));
                        Console.WriteLine();
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Error");
                    Console.WriteLine();
                }
            }
        }
    }
}
